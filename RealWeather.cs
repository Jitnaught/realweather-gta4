﻿using GTA;
using System;
using System.IO;
using System.Net;
using System.Threading;
using System.Windows.Forms;
using System.Xml;

namespace RealWeather
{
    public class RealWeather : Script
    {
        string messageConsoleOnly = "", messageConsoleAndSubtitle = "";

        bool realWeatherEnabled;
        string currentCondition = "";
        string sLocation;
        GTA.Timer timer1, timer2;
        Keys kHoldKey, kPressKey;

        public RealWeather()
        {
            if (!File.Exists(Settings.Filename))
            {
                Settings.SetValue("LOCATION", "SETTINGS", "Chicago, IL");
                Settings.SetValue("TOGGLE_HOLD_KEY", "KEYS", Keys.RControlKey);
                Settings.SetValue("TOGGLE_PRESS_KEY", "KEYS", Keys.W);
                Settings.SetValue("ENABLED_ON_STARTUP", "KEYS", true);
                Settings.Save();
            }

            sLocation = Settings.GetValueString("LOCATION", "SETTINGS", "Chicago, IL");
            kHoldKey = Settings.GetValueKey("TOGGLE_HOLD_KEY", "KEYS", Keys.RControlKey);
            kPressKey = Settings.GetValueKey("TOGGLE_PRESS_KEY", "KEYS", Keys.W);
            realWeatherEnabled = Settings.GetValueBool("ENABLED_ON_STARTUP", "KEYS", true);

            timer1 = new GTA.Timer(10);
            timer1.Tick += timer1_Tick;
            timer1.Start();

            timer2 = new GTA.Timer(5000);
            timer2.Tick += timer2_Tick;
            timer2.Start();

            KeyDown += RealWeather_Script_KeyDown;

            Thread thread = new Thread(new ThreadStart(RealWeather_Thread));
            thread.IsBackground = true;
            thread.Start();
        }

        private void RealWeather_Script_KeyDown(object sender, GTA.KeyEventArgs e)
        {
            if (isKeyPressed(kHoldKey) && e.Key == kPressKey)
            {
                Subtitle("Real Weather " + ((realWeatherEnabled = !realWeatherEnabled) ?  "enabled" : "disabled"));
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (realWeatherEnabled)
            {
                if (messageConsoleOnly != "")
                {
                    PrintConsoleMessage(messageConsoleOnly);
                    messageConsoleOnly = "";
                }

                if (messageConsoleAndSubtitle != "")
                {
                    PrintConsoleAndSubtitleMessage(messageConsoleAndSubtitle);
                    messageConsoleAndSubtitle = "";
                }
            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            SetWeather(currentCondition);
        }

        private void RealWeather_Thread()
        {
            bool done = false;

            while (!done)
            {
                if (realWeatherEnabled)
                {
                    string getCondition = "";
                    try
                    {
                        getCondition = GetCurrentConditions(sLocation);
                    }
                    catch (WebException)
                    {
                        messageConsoleAndSubtitle = "###ERROR### RealWeather does not work when you are offline. If you are online, but still get this error, then please type 'reloadscripts' (without the quotes) in the console, then press ENTER. ###ERROR###";
                        for (uint i = 0; i < 4000; i++)
                        {
                            if (messageConsoleAndSubtitle == "") break;
                            Thread.Sleep(10);
                        }
                        done = true;
                        realWeatherEnabled = false;
                        timer1.Stop();
                        timer2.Stop();
                        timer1.Tick -= timer1_Tick;
                        timer2.Tick -= timer2_Tick;
                        timer1 = null;
                        timer2 = null;
                        KeyDown -= RealWeather_Script_KeyDown;
                    }
                    if (!done)
                    {
                        if (!string.IsNullOrWhiteSpace(getCondition))
                        {
                            if (currentCondition != getCondition)
                            {
                                currentCondition = getCondition;
                                messageConsoleOnly = "#Real Weather# Updated weather: " + CurrentConditionToName(getCondition);
                            }
                            Thread.Sleep(60000);
                        }
                        else Thread.Sleep(5000);
                    }
                }
            }
        }


        private void PrintConsoleAndSubtitleMessage(string message)
        {
            Game.Console.Print(message);
            Game.DisplayText(message);
        }

        private void PrintConsoleMessage(string message)
        {
            Game.Console.Print(message);
        }

        private bool isInGame()
        {
            if (Game.Exists(Player.Character) && Player.Character.isAlive && !GTA.Native.Function.Call<bool>("IS_SCREEN_FADING_IN") && !GTA.Native.Function.Call<bool>("IS_SCREEN_FADED_OUT") && !GTA.Native.Function.Call<bool>("IS_SCREEN_FADING_OUT")) return true;
            return false;
        }

        private string GetCurrentConditions(string location)
        {
            string condition = ""; //"" = error
            XmlReader xmlReader = XmlReader.Create(string.Format("http://api.openweathermap.org/data/2.5/weather?q={0}&mode=xml", location));
            while (xmlReader.Read())
            {
                if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "weather") && xmlReader.HasAttributes)
                {
                    condition = xmlReader.GetAttribute("number");
                    break;
                }
            }

            return condition;
        }

        private string CurrentConditionToName(string condition)
        {
            string name = "Other";
            switch (condition)
            {
                case "200":
                case "201":
                case "202":
                case "210":
                case "211":
                case "212":
                case "221":
                case "230":
                case "231":
                case "232":

                case "960":
                case "961":
                case "962":
                    //World.Weather = Weather.ThunderStorm;
                    name = "Thunderstorm";
                    break;

                case "300":
                case "301":
                case "302":
                case "310":
                case "311":
                case "312":
                case "313":
                case "314":
                case "321":
                    //World.Weather = Weather.Drizzle;
                    name = "Drizzle";
                    break;

                case "500":
                case "501":
                case "502":
                case "503":
                case "504":
                case "511":
                case "520":
                case "521":
                case "522":
                case "531":
                case "600":
                case "601":
                case "602":
                case "611":
                case "612":
                case "615":
                case "616":
                case "620":
                case "621":
                case "622":
                    //World.Weather = Weather.Raining;
                    name = "Raining";
                    break;

                case "701":
                case "711":
                case "721":
                case "731":
                case "741":
                case "751":
                case "761":
                case "762":
                case "771":
                case "781":
                    //World.Weather = Weather.Foggy;
                    name = "Foggy";
                    break;

                case "800":
                case "801":
                case "802":
                    //World.Weather = Weather.Sunny;
                    name = "Sunny";
                    break;

                case "803":
                case "804":
                    //World.Weather = Weather.Cloudy;
                    name = "Cloudy";
                    break;

                case "952":
                case "953":
                case "954":
                case "955":
                    // World.Weather = Weather.SunnyAndWindy;
                    name = "Sunny and windy";
                    break;

                case "956":
                case "957":
                case "958":
                case "959":
                    //World.Weather = Weather.SunnyAndWindy2;
                    name = "Sunny and windy (2)";
                    break;
            }
            return name;
        }

        private void SetWeather(string condition)
        {
            switch (condition)
            {
                case "200":
                case "201":
                case "202":
                case "210":
                case "211":
                case "212":
                case "221":
                case "230":
                case "231":
                case "232":

                case "960":
                case "961":
                case "962":
                    //World.Weather = Weather.ThunderStorm;
                    if (World.Weather != (Weather)7)
                    {
                        GTA.Native.Function.Call("FORCE_WEATHER_NOW", 7);
                    }
                    break;

                case "300":
                case "301":
                case "302":
                case "310":
                case "311":
                case "312":
                case "313":
                case "314":
                case "321":
                    //World.Weather = Weather.Drizzle;
                    if (World.Weather != (Weather)5)
                    {
                        GTA.Native.Function.Call("FORCE_WEATHER_NOW", 5);
                    }
                    break;

                case "500":
                case "501":
                case "502":
                case "503":
                case "504":
                case "511":
                case "520":
                case "521":
                case "522":
                case "531":
                case "600":
                case "601":
                case "602":
                case "611":
                case "612":
                case "615":
                case "616":
                case "620":
                case "621":
                case "622":
                    //World.Weather = Weather.Raining;
                    if (World.Weather != (Weather)4)
                    {
                        GTA.Native.Function.Call("FORCE_WEATHER_NOW", 4);
                    }
                    break;

                case "701":
                case "711":
                case "721":
                case "731":
                case "741":
                case "751":
                case "761":
                case "762":
                case "771":
                case "781":
                    //World.Weather = Weather.Foggy;
                    if (World.Weather != (Weather)6)
                    {
                        GTA.Native.Function.Call("FORCE_WEATHER_NOW", 6);
                    }
                    break;

                case "800":
                case "801":
                case "802":
                    //World.Weather = Weather.Sunny;
                    if (World.Weather != (Weather)0) //1
                    {
                        GTA.Native.Function.Call("FORCE_WEATHER_NOW", 0); //1
                    }
                    break;

                case "803":
                case "804":
                    //World.Weather = Weather.Cloudy;
                    if (World.Weather != (Weather)3)
                    {
                        GTA.Native.Function.Call("FORCE_WEATHER_NOW", 3);
                    }
                    break;

                case "952":
                case "953":
                case "954":
                case "955":
                    // World.Weather = Weather.SunnyAndWindy;
                    if (World.Weather != (Weather)2)
                    {
                        GTA.Native.Function.Call("FORCE_WEATHER_NOW", 2);
                    }
                    break;

                case "956":
                case "957":
                case "958":
                case "959":
                    //World.Weather = Weather.SunnyAndWindy2;
                    if (World.Weather != (Weather)9)
                    {
                        GTA.Native.Function.Call("FORCE_WEATHER_NOW", 9);
                    }
                    break;
            }
        }

        private void Subtitle(string message, int time = 1500)
        {
            GTA.Native.Function.Call("PRINT_STRING_WITH_LITERAL_STRING_NOW", "STRING", message, time, 1);
        }
    }
}
