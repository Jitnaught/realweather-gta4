A mod that sets the weather in GTA IV to be the real, live, weather in a set location, like Chicago, IL, or Las Vegas, NV.

How to install:
Install .NET Scripthook.
Copy the RealWeather.net.dll and ReadWeather.ini to the scripts folder in your GTA IV directory.

Controls:
Right-CTRL + W = Toggle the script

Changeable in the ini file:
The controls.
Whether the script should be enabled on startup or not.
The location to get the weather data from.

How to compile:
I lost the project files after a hard drive failure, so you'll need to:

* create a new C# library/DLL project in Visual Studio
* replace the generated cs file with the cs file in this archive
* download .NET ScriptHook and add a reference to it
* click compile
* change the extension of the generated .dll file to .net.dll
